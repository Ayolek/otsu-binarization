package mainpackage;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;

import javax.imageio.ImageIO;
import javax.naming.LimitExceededException;

import org.w3c.dom.css.CSSPrimitiveValue;
import org.w3c.dom.css.RGBColor;

public class Main {
	public static double THRESHOLD = 0;
	
	public int[][] originalRed;
	public int[][] originalGreen;
	public int[][] originalBlue;
	
	public int[][] greyscaleImage;
	
	public int[][] binaryImage;
	public int[] histogram;
	public int[] luminanceHistogram;
	public int[][] objects;
	public int objectCounts;
	public ArrayList<Integer> objectsList;
	
	public BufferedImage image;
	public static void main(String[] args) throws IOException {
		new Main();
	}
	
	public Main() throws IOException{
		
		image = ImageIO.read(this.getClass().getResource("1_2.jpg"));
		File greyscaleFile = new File("greyscale.png");
		File binaryFile = new File("binary.png");
		originalRed = new int[image.getWidth()][image.getHeight()];
		originalGreen = new int[image.getWidth()][image.getHeight()];
		originalBlue = new int[image.getWidth()][image.getHeight()];
		greyscaleImage = new int[image.getWidth()][image.getHeight()];
		binaryImage = new int[image.getWidth()][image.getHeight()];
		objects = new int[image.getWidth()][image.getHeight()];
		objectCounts = 0;
		objectsList = new ArrayList<Integer>();
		
		createHistogram(image, "original");
		for(int x=0;x<image.getWidth();x++){
			for(int y=0;y<image.getHeight();y++){
				int grey = rgb2gray(image.getRGB(x, y),x,y);
				greyscaleImage[x][y] = grey;
				Color c = new Color(grey,grey,grey);
				image.setRGB(x, y, c.getRGB());
			}			
		}
		findOptimalThreshold(image.getWidth(), image.getHeight());
		//THRESHOLD = 110;
		setOtsuThreshold();
		ImageIO.write(image, "png", greyscaleFile);
		createHistogram(image, "greyscale");
		grey2bin(image, image.getWidth(), image.getHeight());	
		createHistogram(image, "binary");
		fillObjectsMatrix(image.getWidth(), image.getHeight());
		findObjects(image.getWidth(), image.getHeight());
		printResult();
		ImageIO.write(image, "png", binaryFile);
	}
	
	public void printResult(){
		for(int i=0; i<objectsList.size();i++){
			int id = objectsList.get(i);
			System.out.println("Object "+(i+1)+" id = "+
					objectsList.get(i)+" avarage color = "+ avarageColor(id).
					getRed()+" "+avarageColor(id).
					getGreen()+" "+avarageColor(id).
					getBlue()+ " avarage luminance = " + avarageLuminance(id)
					+" middle point: "+middlePoint(id).toString());
		}
	}
	
	public void printPixelRGB(int pixel) {
	    int red = (pixel >> 16) & 0xff;
	    int green = (pixel >> 8) & 0xff;
	    int blue = (pixel) & 0xff;
	    System.out.println("rgb: " + red + ", " + green + ", " + blue);
	  }
	
	public double avarageLuminance(int id){
		double luminance = 0.0;
		int pixelsCount = 0;
		
		for(int i=0;i<image.getWidth();i++)
			for(int j=0; j<image.getHeight();j++){
				if(objects[i][j] == id){
					luminance += greyscaleImage[i][j];
					pixelsCount++;
				}
			}
		return luminance/pixelsCount;
	}
	
	public Color avarageColor(int id){
		double r=0;
		double g=0;
		double b=0;
		int pixelsCount = 0;
		
		for(int i=0;i<image.getWidth();i++)
			for(int j=0; j<image.getHeight();j++){
				if(objects[i][j] == id){
					r += originalRed[i][j];
					g += originalGreen[i][j];
					b += originalBlue[i][j];
					pixelsCount++;
				}
			}
		if(pixelsCount!=0){
			r=r/pixelsCount;
			g=g/pixelsCount;
			b=b/pixelsCount;
			return new Color((int)r, (int)g, (int)b);
		}else{
			return new Color(0);
		}
	}
	
	public Point middlePoint(int id){
		int pixelsCount = 0;
		int x = 0;
		int y = 0;
		
		for(int i=0;i<image.getWidth();i++)
			for(int j=0; j<image.getHeight();j++){
				if(objects[i][j] == id){
					x+=i;
					y+=j;
					pixelsCount++;
				}
			}
		Point p = new Point((int)(x/pixelsCount), (int)(y/pixelsCount));
		image.setRGB(p.x, p.y, new Color(255,0,0).getRGB());
		return p; 
	}
	

	
	public void setOtsuThreshold(){//TODO

		luminanceHistogram();
		int bestThreshold=0;
		double minVariance = calculateWithinClassVariance(0);
		double actualVariance;
		
		for(int localThreshold=1;localThreshold<256;localThreshold++){
			actualVariance = calculateWithinClassVariance(localThreshold);
			if(actualVariance<minVariance){
				minVariance = actualVariance;
				bestThreshold=localThreshold;
			}
		}
		
		THRESHOLD = bestThreshold;
		System.out.println("OtsuThreshold:"+THRESHOLD);
		
	}
	
	public double calculateWithinClassVariance(int threshold){
		double backgroundWeight = 0;
		double backgroundMean = 0;
		double backgroundMeanCount = 0;
		double backgroundVariance = 0;
		
		double foregroundWeight = 0;
		double foregroundMean = 0;
		double foregroundMeanCount = 0;
		double foregroundVariance = 0;
		
		//-----------------------------------
		//background
		//-----------------------------------
		for(int i=0;i<threshold;i++){
			backgroundWeight += luminanceHistogram[i];
		}
		backgroundWeight/=(image.getWidth()*image.getHeight());
		
		for(int i=0;i<threshold;i++){
			backgroundMean += luminanceHistogram[i]*i;
			backgroundMeanCount += luminanceHistogram[i];
		}
		if(backgroundMeanCount!=0)
			backgroundMean = backgroundMean/backgroundMeanCount;
		else backgroundMean = 0;
		
		for(int i=0;i<threshold;i++){
			backgroundVariance += ((i-backgroundMean)*(i-backgroundMean))*luminanceHistogram[i];
		}
		if(backgroundMeanCount!=0)
		backgroundVariance/=backgroundMeanCount;
		else backgroundVariance = 0;
		//-----------------------------------
		//foreground
		//-----------------------------------
		for(int i=threshold;i<256;i++){
			foregroundWeight += luminanceHistogram[i];
		}
		foregroundWeight/=(image.getWidth()*image.getHeight());
		
		for(int i=threshold;i<256;i++){
			foregroundMean += luminanceHistogram[i]*i;
			foregroundMeanCount += luminanceHistogram[i];
		}
		if(foregroundMeanCount!=0)
			foregroundMean = foregroundMean/foregroundMeanCount;
		else foregroundMean = 0;
			
		for(int i=threshold;i<256;i++){
			foregroundVariance += ((i-foregroundMean)*(i-foregroundMean))*luminanceHistogram[i];
		}
		if(foregroundMeanCount!=0)
		foregroundVariance/=foregroundMeanCount;
		else foregroundVariance = 0;
		
//		System.out.println("threshold="+threshold);
//		System.out.println("background weight="+backgroundWeight);
//		System.out.println("background mean="+backgroundMean);
//		System.out.println("background variance="+backgroundVariance);
//		
//		System.out.println("foreground weight="+foregroundWeight);
//		System.out.println("foreground mean="+foregroundMean);
//		System.out.println("foreground variance="+foregroundVariance);
//		System.out.println("wynik="+
//				(backgroundVariance*backgroundWeight+foregroundVariance*foregroundWeight));
		
		return backgroundVariance*backgroundWeight+foregroundVariance*foregroundWeight;
	}
	 
	public int rgb2gray(int pixel, int x, int y){
		int red = (pixel >> 16) & 0xff;
	    int green = (pixel >> 8) & 0xff;
	    int blue = (pixel) & 0xff;
	    
	    double Y = 0.2989 * red + 0.5870 * green + 0.1140 * blue;
	    
	    saveImage(x, y, red, green, blue);
	    
	    return (int)Y;
	}
	
	public void saveImage(int x, int y, int redValue, int greenValue, int blueValue){
		originalRed[x][y] = redValue;
	    originalGreen[x][y] = greenValue;
	    originalBlue[x][y] = blueValue;
	}
	
	public void fillObjectsMatrix(int width, int height){
		for(int x=0; x<width; x++)
			for(int y=0; y<height; y++){
				objects[x][y] = 0;
			}
	}
	
	public void findOptimalThreshold(int width, int height){
		double average = 0;
		double tempTHRESHOLD = 0;
		int backgroundCount = 0;
		int objectCount = 0;
		double backgroundValue = 0.0;
		double objectValue = 0.0;
		for(int x=0; x<width; x++){
			for(int y=0; y<height; y++){
				average += greyscaleImage[x][y];				
			}
		}
		
		THRESHOLD = average/(width*height);
		
		do{	
			backgroundCount = 0;
			objectCount = 0;
			backgroundValue = 0.0;
			objectValue = 0.0;
			
			for(int x=0; x<width; x++){
				for(int y=0; y<height; y++){
					if(greyscaleImage[x][y]<=THRESHOLD){
						objectValue+=greyscaleImage[x][y];
						objectCount++;
					}else{
						backgroundValue+=greyscaleImage[x][y];
						backgroundCount++;
					}
				}
			}			
			tempTHRESHOLD = THRESHOLD;
			THRESHOLD = ((backgroundValue/backgroundCount)+(objectValue/objectCount))/2;

		}while(THRESHOLD!=tempTHRESHOLD);
		System.out.println("THRESHOLD = "+THRESHOLD);
	}
	
	public void grey2bin(BufferedImage image, int width, int height){
		for(int x=0; x<width; x++)
			for(int y=0; y<height; y++){
				if(greyscaleImage[x][y] <= THRESHOLD){
					image.setRGB(x, y, Color.BLACK.getRGB());
					binaryImage[x][y]=0;
				}else{
					image.setRGB(x, y, Color.WHITE.getRGB());
					binaryImage[x][y]=1;
				}
			}
	}
	
	public void createHistogram(BufferedImage image, String name){
		switch(name){
		case "greyscale":{
			gshist(name);
			break;
		}
		case "original":{
			originalhist(name);
			break;
		}
		case "binary":{
			binhist(name);
			break;
		}
		}
	}
	
	public void originalhist(String name){
		int red[] = new int[256];
		int green[] = new int[256];
		int blue[] = new int[256];
		
		for(int i=0;i<256;i++){
			red[i] = 0;
			green[i] = 0;
			blue[i] = 0;
		}
		
		for(int i=0;i<image.getWidth();i++){
			for(int j=0;j<image.getHeight();j++){
				Color c = new Color(image.getRGB(i, j));
				red[c.getRed()]++;
				green[c.getGreen()]++;
				blue[c.getBlue()]++;
			}
		}
		
		File histogramFile = new File("histogram "+name+".txt");
		try {
			PrintWriter os = new PrintWriter(histogramFile);
			
			os.println("value red green blue");
			
			for(int i=0; i<256;i++){
				os.println(i+"\t"+red[i]+"\t"+green[i]+"\t"+blue[i]);
			}
			os.close();
		} catch (FileNotFoundException e) {e.printStackTrace();}
	}
	
	public void binhist(String name){
		histogram = new int[2];
		for(int i=0;i<2;i++){
			histogram[i] = 0;
		}
		
		for(int i=0;i<image.getWidth();i++){
			for(int j=0;j<image.getHeight();j++){
				histogram[binaryImage[i][j]]++;
			}
		}
		
		File histogramFile = new File("histogram "+name+".txt");
		try {
			PrintWriter os = new PrintWriter(histogramFile);
			
			for(int i=0; i<2;i++){
				os.println(i+"\t"+histogram[i]);
			}
			os.close();
		} catch (FileNotFoundException e) {e.printStackTrace();}
	}
	
	public void luminanceHistogram(){
		luminanceHistogram = new int[256];
		for(int i=0;i<256;i++){
			luminanceHistogram[i] = 0;
		}
		
		for(int i=0;i<image.getWidth();i++){
			for(int j=0;j<image.getHeight();j++){
				luminanceHistogram[greyscaleImage[i][j]]++;
			}
		}
	}
	
	public void gshist(String name){
		histogram = new int[256];
		for(int i=0;i<256;i++){
			histogram[i] = 0;
		}
		
		for(int i=0;i<image.getWidth();i++){
			for(int j=0;j<image.getHeight();j++){
				histogram[greyscaleImage[i][j]]++;
			}
		}
		
		File histogramFile = new File("histogram "+name+".txt");
		try {
			PrintWriter os = new PrintWriter(histogramFile);
			
			for(int i=0; i<256;i++){
				os.println(i+"\t"+histogram[i]);
			}
			os.close();
		} catch (FileNotFoundException e) {e.printStackTrace();}
	}
	
	public void findObjects(int width, int height){
		for(int x=1; x<width-1; x++){
			for(int y=1; y<height-1; y++){
				if(binaryImage[x][y]==0){
					objects[x][y] = checkNeighbourhood(x, y);
				}
			}
		}
		
		for(int x=1; x<width-1; x++){
			for(int y=1; y<height-1; y++){
				validate(x,y);
			}
		}
		
		for(int x=0; x<width; x++){
			for(int y=0; y<height; y++){
				if(objects[x][y]!=0)
					if(!objectsList.contains(objects[x][y]))
						objectsList.add(objects[x][y]);
			}
		}
		
	}
	
	public void validate(int x, int y){
		for(int i=x-1;i<x+2;i++){
			for(int j=y-1;j<y+2;j++){
				if(objects[i][j]!=objects[x][y]&&objects[i][j]!=0&&objects[x][y]!=0){
					if(objects[i][j]<objects[x][y])
						repair(objects[i][j],objects[x][y]);
					else
						repair(objects[x][y],objects[i][j]);
				}
			}
		}
	}
	
	public void repair(int idNew, int idOld){
		for(int i=0; i<image.getWidth(); i++){
			for(int j=0; j<image.getHeight(); j++){
				if(objects[i][j]==idOld)
					objects[i][j] = idNew;
			}
		}
	}
	
	public int checkNeighbourhood(int x, int y){

		for(int i=x-1;i<x+2;i++){
			for(int j=y-1;j<y+2;j++){
				if(objects[i][j]!=0){
					return objects[i][j];					
				}
			}
		}
		
		objectCounts++;
		return objectCounts;
	}
}
